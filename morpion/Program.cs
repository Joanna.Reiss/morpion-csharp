﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int line, column;
            string input;
            char player = 'X';
            char[,] board = new char[3, 3]; // Create board 3 by 3
            Init(board);
            bool end = false;
            int round = 0;


            //display(board);
            while (!end)
            {
                Display(board);

                /*
                 * Verify if the number is between 0 and 2
                 */
                Console.Write("Which line do you choose: ");
                input = Console.ReadLine();
                if (IsInputSanitized(input))
                {
                    line = Convert.ToInt32(input);
                }
                else
                {
                    continue;
                }

                Console.Write("Which column do you choose: ");
                input = Console.ReadLine();
                if (IsInputSanitized(input))
                {
                    column = Convert.ToInt32(input);
                }
                else
                {
                    continue;
                }

                /*
                 * Check whether the player do a legal move or not
                 */
                if (IsACorrectMove(board, line, column, player))
                {
                    board[line, column] = player; // Put the sign in the board
                }
                else
                {
                    Console.WriteLine("Are you sure know how to play...noob ?");
                    continue;
                }

                /*
                 * Check whether it is a victory or not
                 */
                if (IsVictory(board, player))
                {
                    end = true;
                }

                round = round + 1;
                if (round == 9)
                {
                    Console.WriteLine("It's a draw");
                    end = true;
                }

                player = ChangePlayer(player);
            }

            Console.Clear();
            Display(board);
            Console.ReadLine();
        }

        static bool IsACorrectMove(char[,] board, int line, int column, char player)
        {
            if (board[line, column] != ' ')
            {
                return false;
            }

            return true;
        }



        // Verify  the user's entry
        static bool IsInputSanitized(string input)
        {
            if (input.Length == 0 || input.Length > 1) // Verify that one number is entered
            {
                Console.WriteLine("Enter ONE number...Are you sure of what yu're actually doing ?");
                return false;
            }

            if (!int.TryParse(input, out var result)) //Verify that a number is entered
            {
                Console.WriteLine("Are you sure you know what's a number ?");
                return false;
            }

            if (result < 0 || result > 2) // Verify that the number is between 0 and 2
            {
                Console.WriteLine("Can you respect limits?");
                return false;
            }

            return true;
        }

        static bool IsVictory(char[,] board, char player)
        {
            if (player == board[0, 0] && player == board[0, 1] && player == board[0, 2])
            {
                Console.WriteLine(String.Format("{0} win!", player));
                return true;
            }

            if (player == board[1, 0] && player == board[1, 1] && player == board[1, 2])
            {
                Console.WriteLine(String.Format("{0} win!", player));
                return true;
            }

            if (player == board[2, 0] && player == board[2, 1] && player == board[2, 2])
            {
                Console.WriteLine(String.Format("{0} win!", player));
                return true;
            }

            if (player == board[0, 0] && player == board[1, 0] && player == board[2, 0])
            {
                Console.WriteLine(String.Format("{0} win!", player));
                return true;
            }

            if (player == board[0, 1] && player == board[1, 1] && player == board[2, 1])
            {
                Console.WriteLine(String.Format("{0} win!", player));
                return true;
            }

            if (player == board[0, 2] && player == board[1, 2] && player == board[2, 2])
            {
                Console.WriteLine(String.Format("{0} win!", player));
                return true;
            }

            if (player == board[0, 0] && player == board[1, 1] && player == board[2, 2])
            {
                Console.WriteLine(String.Format("{0} win!", player));
                return true;
            }

            if (player == board[0, 2] && player == board[1, 1] && player == board[2, 0])
            {
                Console.WriteLine(String.Format("{0} win!", player));
                return true;
            }

            return false;
        }

        static char ChangePlayer(char actualplayer) // Change player every turn
        {
            if (actualplayer == 'X')
            {
                return 'O';
            }
            else
            {
                return 'X';
            }
        }

        // Initialization of the board
        static void Init(char[,] board)
        {
            for (int line = 0; line < 3; line++)
            {
                for (int column = 0; column < 3; column++)
                {
                    board[line, column] = ' ';
                }
            }
        }

        //Display the board
        static void Display(char[,] board)
        {
            
            Console.WriteLine("  | 0 | 1 | 2 |"); // Header of the board
            for (int line = 0; line < 3; line++)
            {
                Console.Write(String.Format("{0} | ", line)); //String.Format replace a concatenation

                for (int column = 0; column < 3; column++)
                {
                    Console.Write(board[line, column]);
                    Console.Write(" | ");
                }

                Console.WriteLine();
            }
        }
    }
}